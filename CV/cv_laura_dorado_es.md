# Curriculum Vitae - Laura Dorado

>¿Porqué esperar a hacer algo, si puedes hacerlo ya? 

<img src="img/sophia-floersch.jpg" height="200"/>

Estudiante de Formación Profesional de Grado Superior de **Desarrollo de Aplicaciones Multiplataforma**.

Si deseas contactar conmigo puedes mandarme un [correo]("mailto:ldorbar683@g.educaand.es").

## Experiencia profesional

+ Sevilla Systems (03/2019-11/2021): data engineer
  + Prodiel
    + Implantación de software.
  + Persán S.A
    + Implantación de software.
    + Migración de flujos.

## Títulos académicos

1. Título de Educación Secundaria Obligatoria.
2. Título de Bachillerato Científico-Tecnológico obtenido en [IES Tartessos]("https://www.juntadeandalucia.es/averroes/centros-tic/41000880/helvia/sitio/") de Camas, Sevilla en junio de 2021.

## Habilidades y destrezas

| Habilidades | Descripción                            | Experiencia                        |
|-------------|----------------------------------------|------------------------------------|
| Python      | Nivel avanzado de escritura            | :star:                             |
| JavaScript  | Creación y estilización de páginas web | :star: :star: :star:               |
| CSS         | Creación de páginas web                | :star: :star: :star: :star: :star: |
| Java        | Creación de programas                  | :star: :star: :star: :star: :star: |

## Ejemplo de código Java
```java
package com.mycompany.mavenproject;

public class Mavenproject {

    public static void main(String[] args) {
        System.out.println("Hello World!");

        int numeroAleatorio = (int) (Math.random()*10+0);
        System.out.println(numeroAleatorio);
    }
}
```